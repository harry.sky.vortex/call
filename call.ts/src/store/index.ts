import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from '@redux-saga/core';
import { callDialogReducer } from './reducers/CallDialog';
import { callFormReducer } from './reducers/CallDialog/Form';
import { callStatusReducer } from './reducers/CallDialog/Status';
import { screenInfoReducer } from './reducers/ScreenInfo';
import { sendCallNotificationSaga } from './sagas';
import { INITIAL_STATE, AppState } from './State';

const REDUCERS = [
  screenInfoReducer,
  callDialogReducer,
  callFormReducer,
  callStatusReducer,
];

// tslint:disable-next-line: no-any
function rootReducer(state = INITIAL_STATE, action: any): AppState {
  REDUCERS.forEach(reducer => (state = reducer(state, action)));
  return state;
}

const SAGA = createSagaMiddleware();

export const APP_STORE = createStore(rootReducer, applyMiddleware(SAGA));

SAGA.run(sendCallNotificationSaga);
