import { Dispatch } from 'redux';
import { callDialogOpen } from 'store/actions/CallDialog';

export interface Props {
  callDialogOpen(): void;
}

export function mapDispatchToProps(dispatch: Dispatch): Props {
  return { callDialogOpen: () => dispatch(callDialogOpen) };
}
