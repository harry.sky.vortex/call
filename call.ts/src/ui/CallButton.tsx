import CallIcon from '@material-ui/icons/Call';
import Fab from '@material-ui/core/Fab';
import * as React from 'react';
import { connect } from 'react-redux';
import 'ui/css/CallButton.css';
import { Props, mapDispatchToProps } from 'store/ui/CallButton';

export class CallButton extends React.PureComponent<Props> {
  render() {
    return (
      <Fab
        className="call_button"
        onClick={this.props.callDialogOpen}
        color="primary"
        variant="extended"
        aria-label="call"
      >
        <CallIcon />
        Call
      </Fab>
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  null,
  mapDispatchToProps
)(CallButton);
